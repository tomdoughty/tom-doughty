//This is the service worker with the Cache-first network

var CACHE = 'pwabuilder-precache';

var precacheFiles = [
		'https://www.tomdoughty.co.uk',
		'https://www.tomdoughty.co.uk/robots.txt',
		'https://www.tomdoughty.co.uk/favicon.ico',
		'https://www.tomdoughty.co.uk/manifest.json',
		'https://cdn.ampproject.org/v0.js',
		'https://cdn.ampproject.org/v0/amp-experiment-0.1.js',
		'https://cdn.ampproject.org/rtv/012006180239003/v0/amp-auto-lightbox-0.1.js',
		'https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js'
	];

self.addEventListener('install', (e) => {
	e.waitUntil(
		precache().then(() => self.skipWaiting())
	);
});

self.addEventListener('activate', () => self.clients.claim());

self.addEventListener('fetch', (e) => {
	e.respondWith(
		fromCache(e.request).catch(fromServer(e.request))
	);
	e.waitUntil(update(e.request));
});

function precache() {
	return caches.open(CACHE)
		.then(cache => {
			return cache.addAll(precacheFiles);
		});
}

function fromCache(request) {
	return caches.open(CACHE)
  		.then(cache => {
			return cache.match(request)
				.then(matching => matching || Promise.reject('no-match'));
  		});
}


function update(request) {
	if (request.cache === 'only-if-cached' && request.mode !== 'same-origin') return;
	return caches.open(CACHE)
		.then(cache => fetch(request)
		.then(response => cache.put(request, response)));
}

function fromServer(request){
	if (request.cache === 'only-if-cached' && request.mode !== 'same-origin') return;
	return fetch(request)
		.then(response => response);
}